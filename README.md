# Jocasta Nu 
## https://starwars.fandom.com/es/wiki/Jocasta_Nu

A python script to retrieve data from the starWars API: https://swapi.dev/

Language: Python3
Python Modules: argparse, json, requests

Install:  
1. Clone repo: git clone https://gitlab.com/RojasRafael/jocasta-nu.git
2. Install required packages:
  * pip install argparse
  * pip install requests
  * (json is part of default python3 package)
3. Run: ./jocasta.py -h


jocasta help:

    usage: jocasta.py [-h] [--listPeople {1,2,3,4,5,6}] [--listFilms] [--shipsinMovie {1,2,3,4,5,6}]
                      [--hyperdriveGreater HYPERDRIVEGREATER] [--findShips lower upper]
    optional arguments:
      -h, --help            show this help message and exit
      --listPeople {1,2,3,4,5,6}, -lp {1,2,3,4,5,6}
                            List Start Wars Characters for each movie, even Jar jar. :(
      --listFilms, -lf      List star wars films in release chronological order, even the prequels :(
      --shipsinMovie {1,2,3,4,5,6}, -sim {1,2,3,4,5,6}
                            list all ships in a given movie
      --hyperdriveGreater HYPERDRIVEGREATER, -hdr-gr HYPERDRIVEGREATER
                            Search ships with an hyperdrive rating greating than given value
      --findShips lower upper, -fs lower upper
                            find ships with a crew between two ranges

Examples:

Find all ships that appeared in Return of the Jedi

    ./jocasta.py -sim 3
    Startships in Movie:  Return of the Jedi
    CR90 corvette
    Star Destroyer
    Millennium Falcon
    Y-wing
    X-wing
    Executor
    Rebel transport
    Imperial shuttle
    EF76 Nebulon-B escort frigate
    Calamari Cruiser
    A-wing
    B-wing

Find all ships that have a hyperdrive rating >= 1.0

    ./jocasta.py -hdr-gr 1.0
    ships with HyperDrive rating equal or higher than:  1.0
    CR90 corvette  -  2.0
    Star Destroyer  -  2.0
    Sentinel-class landing craft  -  1.0
    Death Star  -  4.0
    Y-wing  -  1.0
    X-wing  -  1.0
    TIE Advanced x1  -  1.0
    Executor  -  2.0
    Rebel transport  -  4.0

Find all ships that have crews between 3 and 100

    ./jocasta.py -fs 3 100
    ships with crew capacity between the range of:  3  and:  100
    Ship crew Sentinel-class landing craft  within range:  5
    Ship crew Millennium Falcon  within range:  4
    Ship crew Rebel transport  within range:  6
